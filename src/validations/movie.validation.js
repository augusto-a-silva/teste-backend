import Joi from 'joi'

const registerMovieValidateSchema = Joi.object({
  title: Joi.string(),
  genre: Joi.string(),
  director: Joi.string(),
  writers: Joi.array(),
  actors: Joi.array(),
  rates: Joi.array(),
  synopsis: Joi.string(),
  year: Joi.number()
})

export { registerMovieValidateSchema }
