import Joi from 'joi'

const registerAdminValidateSchema = Joi.object({
  firstName: Joi.string()
    .required(),
  lastName: Joi.string()
    .required(),
  username: Joi.string()
    .alphanum()
    .required(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),
  password: Joi.string()
    // Join.pattern only accepts regex with the RegExp constructor
    // eslint-disable-next-line prefer-regex-literals
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
    .required(),
  isAdmin: Joi.boolean()

})

const updateAdminValidateSchema = Joi.object({
  firstName: Joi.string(),
  lastName: Joi.string(),
  username: Joi.string()
    .alphanum(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
  description: Joi.string(),
  password: Joi.string()
    // Join.pattern only accepts regex with the RegExp constructor
    // eslint-disable-next-line prefer-regex-literals
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
  isAdmin: Joi.boolean()
})

export { registerAdminValidateSchema, updateAdminValidateSchema }
