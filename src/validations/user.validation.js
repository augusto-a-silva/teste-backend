import Joi from 'joi'

const registerUserValidateSchema = Joi.object({
  firstName: Joi.string()
    .required(),
  lastName: Joi.string()
    .required(),
  username: Joi.string()
    .alphanum()
    .required(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),
  password: Joi.string()
    // Join.pattern only accepts regex with the RegExp constructor
    // eslint-disable-next-line prefer-regex-literals
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
    .required()
})

const updateUserValidateSchema = Joi.object({
  firstName: Joi.string(),
  lastName: Joi.string(),
  username: Joi.string()
    .alphanum(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
  description: Joi.string()
})

const updateUserPasswordValidateSchema = Joi.object({
  currentPassword: Joi.string()
    // Join.pattern only accepts regex with the RegExp constructor
    // eslint-disable-next-line prefer-regex-literals
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
    .required(),
  newPassword: Joi.string()
    // Join.pattern only accepts regex with the RegExp constructor
    // eslint-disable-next-line prefer-regex-literals
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
    .required(),
  confirmPassword: Joi.string()
    // Join.pattern only accepts regex with the RegExp constructor
    // eslint-disable-next-line prefer-regex-literals
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
    .required()
})

export { registerUserValidateSchema, updateUserValidateSchema, updateUserPasswordValidateSchema }
