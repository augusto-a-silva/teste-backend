import bcrypt from 'bcrypt'
import consola from 'consola'
import mongoose from 'mongoose'
import { User } from '../models/user.model.js'
import { Movie } from '../models/movie.model.js'

/**
 * Connects to the MongoDB instance according to a given URI.
 *
 * @param mongoUri a string with the mongodb instance connection URI.
 */
export function connect (mongoUri) {
  try {
    mongoose.connect(
      mongoUri,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
      },
      () => {
        if (process.env.NODE_ENV !== 'test') {
          consola.success({
            message: 'Conected to MongoDB!',
            badge: true
          })
        }
      }
    )
  } catch (error) {
    consola.error({
      message: `Error while connecting with MongoDB!\n ${error}`,
      badge: true
    })
  }
}

/**
 * Creates the first administrator of the system.
 *
 * A initial administrator should be created on the first run of the application
 * to be used later on to create the other admins.
 *
 * @param firstAdmin an object with the sysadmin data.
 */
export async function createFirstAdmin (firstAdmin) {
  try {
    const salt = await bcrypt.genSalt(10)

    const hashPassword = await bcrypt.hash(firstAdmin.password, salt)

    firstAdmin.password = hashPassword

    const newAdmin = await User.create(firstAdmin)

    if (process.env.NODE_ENV === 'development') {
      consola.success({
        message: `Done! Created the sysadmin with ID ${newAdmin._id}`,
        badge: true
      })
    }
  } catch (error) {
    if (process.env.NODE_ENV === 'development') {
      if (error.name === 'MongoError' && error.code === 11000) {
        consola.info({
          message: 'The sysadmin was already created on the first run!',
          badge: true
        })
      } else {
        consola.error({
          message: `The following error was occurred:\n ${error}`,
          badge: true
        })
      }
    }
  }
}

/**
 * Creates two users on the user collection.
 *
 * It should only be called in development mode, as the generater users
 * are only for testing purposes.
 */
export async function seedUsersCollection () {
  const users = [
    {
      firstName: 'João',
      lastName: 'Silva',
      username: 'joaosilva',
      email: 'joaosilva@email.com',
      password: 'joao123',
      description: 'Me chamo João e sou um cinéfilo de carteirinha.'
    },
    {
      firstName: 'Maria',
      lastName: 'Silva',
      username: 'mariasilva',
      email: 'mariasilva@email.com',
      password: 'maria123',
      description: 'Olá, eu sou Maria e sou apaixonada em filmes.'
    }
  ]

  try {
    for (const user of users) {
      const foundUser = await User.findOne({ username: user.username })

      if (foundUser) {
        if (process.env.NODE_ENV === 'development') {
          consola.info({
            message: `The seeded user ${user.username} was created on the first run!`,
            badge: true
          })
        }
      } else {
        const salt = await bcrypt.genSalt(10)

        const hashPassword = await bcrypt.hash(user.password, salt)

        user.password = hashPassword

        const newUser = await User.create(user)

        if (process.env.NODE_ENV === 'development') {
          consola.success({
            message: `Done! Created the user ${newUser.username} with ID ${newUser._id}`,
            badge: true
          })
        }
      }
    }
  } catch (error) {
    if (process.env.NODE_ENV === 'development') {
      consola.error({
        message: `The following error was occurred:\n ${error}`,
        badge: true
      })
    }
  }
}

export async function seedMoviesCollection () {
  try {
    const moviesFound = await Movie.find({})

    if (moviesFound.length === 0) {
      const movies = [
        {
          title: 'Mank',
          genre: 'Biography',
          director: 'David Fincher',
          writers: ['Jack Fincher'],
          actors: ['Gary Oldman', 'Amanda Seyfried', 'Lily Collins'],
          synopsis: "1930's Hollywood is reevaluated through the eyes of scathing social critic and alcoholic screenwriter Herman J. Mankiewicz as he races to finish the screenplay of Cidadão Kane (1941).",
          year: 2020
        },
        {
          title: 'Soul',
          genre: 'Animation',
          director: 'Pete Docter',
          writers: ['Kemp Powers'],
          actors: ['Jamie Foxx', 'Tina Fey', 'Graham Norton', 'Rachel House'],
          synopsis: 'After landing the gig of a lifetime, a New York jazz pianist suddenly finds himself trapped in a strange land between Earth and the afterlife..',
          year: 2020
        },
        {
          title: 'A Subida',
          genre: 'Biography',
          director: 'David Fincher',
          writers: ['Jack Fincher'],
          actors: ['Gary Oldman', 'Amanda Seyfried'],
          synopsis: 'A look at the friendship between two guys that spans over many years.',
          year: 2019
        }
      ]
      await Movie.insertMany(movies)
    }

    if (process.env.NODE_ENV === 'development') {
      consola.success({
        message: 'Done! Inserted a list of movies for testing.',
        badge: true
      })
    }
  } catch (error) {
    if (process.env.NODE_ENV === 'development') {
      consola.error({
        message: `The following error was occurred:\n ${error}`,
        badge: true
      })
    }
  }
}
