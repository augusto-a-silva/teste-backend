import jwt from 'jsonwebtoken'
import AuthorizationError from '../errors/authorization.error.js'
import UserError from '../errors/user.error.js'
import AdminError from '../errors/admin.error.js'
import { User } from '../models/user.model.js'

function isBearerValid (req, res, next) {
  let token = req.headers.authorization

  try {
    if (!token) {
      throw new AuthorizationError('The client has not send the auth token.')
    }

    if (!token.startsWith('Bearer')) {
      throw new AuthorizationError('The client has not send a bearer token.')
    }

    token = token.slice(7, token.length)

    jwt.verify(token, process.env.TOKEN_SECRET)

    next()
  } catch (error) {
    if (error.name === 'AuthorizationError') {
      return res.status(401).send({
        data: {
          error: {
            name: 'Authorization error',
            message: error.message
          }
        }
      })
    }

    return res.status(500).send({
      data: {
        error: {
          name: 'Internal error',
          message: 'The token could not be verified.'
        }
      }
    })
  }
}

async function isBearerFromSameUser (req, res, next) {
  let token = req.headers.authorization
  const paramsId = req.params.id

  token = token.slice(7, token.length)

  try {
    const decodedJwt = jwt.verify(token, process.env.TOKEN_SECRET)

    const user = await User.findById(paramsId)

    if (!user) {
      throw new UserError('The user to compare with the token was not found.')
    }

    if (user._id.toString() !== decodedJwt.sub) {
      throw new UserError('The user related to the given token is not the same from the current user.')
    }

    next()
  } catch (error) {
    if (error.name === 'UserError') {
      return res.status(401).send({
        data: {
          error: {
            name: 'User error',
            message: error.message
          }
        }
      })
    }

    return res.status(500).send({
      data: {
        error: {
          name: 'Internal error',
          message: 'A server error prevented the token validation against current user.'
        }
      }
    })
  }
}

async function isAdmin (req, res, next) {
  let token = req.headers.authorization

  token = token.slice(7, token.length)

  try {
    const decodedJwt = jwt.verify(token, process.env.TOKEN_SECRET)

    const admin = await User.findById(decodedJwt.sub)

    if (!admin) {
      throw new AdminError('The user to compare with the token was not found.')
    }

    if (!admin.isAdmin) {
      throw new AdminError('The user related to the given token is not the same from the current user.')
    }

    next()
  } catch (error) {
    if (error.name === 'AdminError') {
      return res.status(403).send({
        data: {
          error: {
            name: 'User error',
            message: error.message
          }
        }
      })
    }

    return res.status(500).send({
      data: {
        error: {
          name: 'Internal error',
          message: 'A server error prevented the token validation against current user.'
        }
      }
    })
  }
}

async function isNotAdmin (req, res, next) {
  let token = req.headers.authorization

  token = token.slice(7, token.length)

  try {
    const decodedJwt = jwt.verify(token, process.env.TOKEN_SECRET)

    const admin = await User.findById(decodedJwt.sub)

    if (!admin) {
      throw new AdminError('The user to compare with the token was not found.')
    }

    if (admin.isAdmin) {
      throw new AdminError('The user is admin and can not rate a movie.')
    }

    next()
  } catch (error) {
    if (error.name === 'AdminError') {
      return res.status(403).send({
        data: {
          error: {
            name: 'User error',
            message: error.message
          }
        }
      })
    }

    return res.status(500).send({
      data: {
        error: {
          name: 'Internal error',
          message: 'A server error prevented the token validation against current user.'
        }
      }
    })
  }
}
export { isBearerValid, isBearerFromSameUser, isAdmin, isNotAdmin }
