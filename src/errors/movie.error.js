export default class MovieError extends Error {
  // eslint-disable-next-line space-before-function-paren
  constructor(message, statusCode) {
    super(message)
    this.name = 'MovieError'
    this.statusCode = statusCode
  }
}
