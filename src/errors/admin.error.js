export default class AdminError extends Error {
  constructor(message) {
    super(message)
    this.name = 'UserError'
  }
}
