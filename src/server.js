import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import helment from 'helmet'
import consola from 'consola'
import morgan from 'morgan'
import { config } from 'dotenv'
import * as routes from './routes/index.routes.js'
import * as mongoDb from './configs/mongo.config.js'

config()

const app = express()
const PORT = parseInt(process.env.PORT, 10)
const mongoUri = process.env.MONGODB_URI
const firstAdmin = JSON.parse(process.env.FIRST_ADMIN)

// MongoDB bootstrap functions.
mongoDb.connect(mongoUri)
mongoDb.createFirstAdmin(firstAdmin)
mongoDb.seedUsersCollection()
mongoDb.seedMoviesCollection()

// Global middlewares section.
// Applies cors on all routes.
app.use(cors())

// Applies helmet on all routes.
app.use(helment())

// Applies JSON body parser on all routes.
app.use(bodyParser.json())

// Applies morgan http logger only in dev mode.

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}
// Initializes the routes.
routes.initialize(app)

app.listen(PORT, () => {
  consola.success({
    message: `Servidor rodando em http://localhost:${PORT}`,
    badge: true
  })
})

export default app
