import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
// import consola from 'consola'
import { User } from '../models/user.model.js'
import { registerUserValidateSchema, updateUserValidateSchema, updateUserPasswordValidateSchema } from '../validations/user.validation.js'
import UserError from '../errors/user.error.js'

export async function registerUser (req, res) {
  const reqUser = req.body

  try {
    await registerUserValidateSchema.validateAsync(reqUser)

    const salt = await bcrypt.genSalt(10)

    const hashPassword = await bcrypt.hash(reqUser.password, salt)

    reqUser.password = hashPassword

    const newUser = await User.create(reqUser)

    // Creates and signes a JWT for the newly created user.
    const accessToken = jwt.sign(
      {
        sub: newUser._id,
        firstName: newUser.firstName,
        lastName: newUser.lastName,
        username: newUser.username,
        type: 'access'
      },
      process.env.TOKEN_SECRET,
      {
        expiresIn: '12h'
      }
    )

    return res.status(201).send({
      data: {
        message: `The user ${newUser.username} has been created and logged in!`,
        user: {
          id: newUser._id,
          fullName: newUser.firstName + ' ' + newUser.lastName,
          username: newUser.username,
          email: newUser.email
        },
        token: `Bearer ${accessToken}`
      }
    })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(400).send({
        data: {
          error: {
            name: 'Validation error',
            message: error.details[0].message
          }
        }
      })
    }

    if (error.name === 'MongoError') {
      return res.status(500).send({
        data: {
          error: {
            name: 'Mongo error',
            message: error.message
          }
        }
      })
    }
    return res.status(500).send({
      data: {
        error: {
          error: error.name,
          message: 'An internal error occured.'
        }
      }
    })
  }
}

export async function updateUser (req, res) {
  const userId = req.params.id
  const reqUser = req.body
  const query = {
    _id: userId
  }

  try {
    await updateUserValidateSchema.validateAsync(reqUser)

    reqUser.updatedAt = new Date()

    // TODO: updateResponse.n and updateResponse.nModified must be equal in tests!
    const updateResponse = await User.updateOne(query, reqUser)

    if (updateResponse !== 1 && updateResponse.nModified !== 1) {
      throw new UserError('The user could not be updated!')
    }

    res.status(200).send({
      data: {
        message: `Done! Updated user with ID:${userId}`
      }
    })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(400).send({
        data: {
          error: {
            name: 'Validation error',
            message: error.details[0].message
          }
        }
      })
    }

    if (error.name === 'UserError') {
      return res.status(403).send({
        data: {
          error: {
            name: 'User error',
            message: error.message
          }
        }
      })
    }
    return res.status(500).send({
      data: {
        error: error.name,
        message: 'An internal error occured.'
      }
    })
  }
}

export async function updateUserPassword (req, res) {
  const userId = req.params.id
  const data = req.body
  const query = {
    _id: userId
  }

  try {
    // Joi validate req.body
    await updateUserPasswordValidateSchema.validateAsync(data)

    if (data.newPassword !== data.confirmPassword) {
      throw new UserError('The passwords does not match!')
    }

    const user = await User.findById(userId)

    const isCurrentPasswordValid = await bcrypt.compare(data.currentPassword, user.password)

    if (!isCurrentPasswordValid) {
      throw new UserError('The current given password is incorrect!')
    }

    const isNewPasswordSame = await bcrypt.compare(data.newPassword, user.password)

    if (isNewPasswordSame) {
      throw new UserError('The new given can not be the same as the current one!')
    }

    const salt = await bcrypt.genSalt(10)

    const hashPassword = await bcrypt.hash(data.newPassword, salt)

    user.password = hashPassword

    const updateResponse = await User.updateOne(query, user)

    if (updateResponse !== 1 && updateResponse.nModified !== 1) {
      throw new UserError('The user could not be updated!')
    }

    res.status(200).send({
      data: {
        message: `Done! Updated user with ID:${userId}`
      }
    })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(400).send({
        data: {
          error: {
            name: 'Validation error',
            message: error.details[0].message
          }
        }
      })
    }

    if (error.name === 'UserError') {
      return res.status(400).send({
        data: {
          error: {
            name: 'User error',
            message: error.message
          }
        }
      })
    }

    return res.status(500).send({
      data: {
        error: error.name,
        message: 'An internal error occured.'
      }
    })
  }
}

export async function softDeleteUser (req, res) {
  const userId = req.params.id
  const query = {
    _id: userId
  }

  try {
    const user = await User.findById(userId)
    user.updatedAt = new Date()
    user.deletedAt = new Date()

    const updateResponse = await User.updateOne(query, user)

    if (updateResponse !== 1 && updateResponse.nModified !== 1) {
      throw new UserError('The user could not be deleted!')
    }

    res.status(200).send({
      data: {
        message: `Done! deleted user with ID:${userId}`
      }
    })
  } catch (error) {
    if (error.name === 'UserError') {
      return res.status(403).send({
        data: {
          error: {
            name: 'User error',
            message: error.message
          }
        }
      })
    }

    return res.status(500).send({
      data: {
        error: error.name,
        message: 'An internal error occured.'
      }
    })
  }
}
