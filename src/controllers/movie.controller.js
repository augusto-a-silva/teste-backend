import { Movie } from '../models/movie.model.js'
import MovieError from '../errors/movie.error.js'
import { registerMovieValidateSchema } from '../validations/movie.validation.js'

export async function searchMovie (req, res) {
  const movieId = req.params.id

  try {
    const movie = await Movie.findById(movieId)

    if (!movie) {
      throw new MovieError('No movie was found', 404)
    }

    return res.status(200).send({
      data: {
        movie
      }
    })
  } catch (error) {
    if (error.name === 'MovieError') {
      return res.status(error.statusCode).send({
        data: {
          error: 'Movie error',
          message: error.message
        }
      })
    }
    return res.status(500).send({
      data: {
        error: error.name,
        message: 'An internal error occured.'
      }
    })
  }
}

export async function searchMovies (req, res) {
  const searchQuery = req.query
  const query = {}

  if (searchQuery.director) {
    query.director = searchQuery.director
  }

  if (searchQuery.title) {
    query.title = searchQuery.title
  }

  if (searchQuery.genre) {
    query.genre = searchQuery.genre
  }

  if (searchQuery.actors) {
    query.actors = { $in: searchQuery.actors }
  }

  try {
    const movies = await Movie.find(query)

    if (movies.length === 0) {
      throw new MovieError('No movie was found', 404)
    }

    return res.status(200).send({
      data: {
        movies
      }
    })
  } catch (error) {
    if (error.name === 'MovieError') {
      return res.status(error.statusCode).send({
        data: {
          error: 'Movie error',
          message: error.message
        }
      })
    }
    return res.status(500).send({
      data: {
        error: error.name,
        message: 'An internal error occured.'
      }
    })
  }
}

export async function registerMovie (req, res) {
  const reqMovie = req.body

  await registerMovieValidateSchema.validateAsync(reqMovie)

  try {
    const newMovie = await Movie.create(reqMovie)

    return res.status(201).send({
      data: {
        message: `The movie ${reqMovie.title} from the director ${reqMovie.director} was created with ID ${newMovie._id}`,
        movie: newMovie
      }
    })
  } catch (error) {
    console.log(error)
    if (error.name === 'ValidationError') {
      return res.status().send({
        data: {
          error: 'Validation error',
          message: error.details[0].message
        }
      })
    }
    return res.status(500).send({
      data: {
        error: error.name,
        message: 'An internal error occured.'
      }
    })
  }
}

export async function rateMovie (req, res) {
  const movieId = req.params.id
  const reqRate = req.body
  const query = {
    _id: movieId
  }

  try {
    const movie = await Movie.findById(movieId)

    if (!movie) {
      throw new MovieError('No movie was found', 404)
    }

    movie.rates.push({
      ratedBy: reqRate.ratedBy,
      rating: reqRate.rating
    })

    let sumRate = 0

    movie.rates.forEach(rate => {
      sumRate = rate.rating + sumRate
    })

    const avgRate = sumRate / movie.rates.length

    movie.avgRate = avgRate

    const updateResponse = await Movie.updateOne(query, movie)

    if (updateResponse !== 1 && updateResponse.nModified !== 1) {
      throw new MovieError('The movie could not be rated!', 400)
    }

    return res.status(200).send({
      data: {
        movie
      }
    })
  } catch (error) {
    if (error.name === 'MovieError') {
      return res.status(error.statusCode).send({
        data: {
          error: 'Movie error',
          message: error.message
        }
      })
    }
    return res.status(500).send({
      data: {
        error: error.name,
        message: 'An internal error occured.'
      }
    })
  }
}
