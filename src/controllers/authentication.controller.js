import bcrypt from 'bcrypt'
// import consola from 'consola'
import jwt from 'jsonwebtoken'
import { User } from '../models/user.model.js'
import AuthenticationError from '../errors/authentication.error.js'

export async function login (req, res) {
  // TODO add Joi validation data.
  try {
    const user = await User.findOne({ email: req.body.email })

    if (!user) {
      throw new AuthenticationError('The given email does not correspond for a registered user!', 404)
    }

    const isPasswordValid = await bcrypt.compare(req.body.password, user.password)

    if (!isPasswordValid) {
      // TODO: Throw a custom invalid error here and retur at the catch
      throw new AuthenticationError('The given password is invalid!', 401)
    }

    // Creates and signes a jwt.
    const accessToken = jwt.sign(
      {
        sub: user._id,
        firstName: user.firstName,
        lastName: user.lastName,
        username: user.username,
        type: 'access'
      },
      process.env.TOKEN_SECRET,
      {
        expiresIn: '12h'
      }
    )

    return res.status(200).send({
      data: {
        message: `The user ${user.username} has been logged in!`,
        user: {
          fullName: user.firstName + user.lastName,
          username: user.username,
          email: user.email
        },
        token: `Bearer ${accessToken}`
      }
    })
  } catch (error) {
    if (error.name === 'AuthenticationError' && error.statusCode === 404) {
      return res.status(404).send({
        data: {
          error: {
            name: 'Authentication error',
            message: error.message
          }
        }
      })
    }

    if (error.name === 'AuthenticationError' && error.statusCode === 401) {
      return res.status(401).send({
        data: {
          error: {
            name: 'Authentication error',
            message: error.message
          }
        }
      })
    }

    return res.status(500).send({
      data: {
        error: error.name,
        message: 'An internal error occured.'
      }
    })
  }
}

// export async function logout (req, res) => {

//   }
