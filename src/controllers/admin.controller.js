import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { User } from '../models/user.model.js'
import { registerAdminValidateSchema, updateAdminValidateSchema } from '../validations/admin.validation.js'
import AdminError from '../errors/admin.error.js'

export async function registerAdmin (req, res) {
  const reqAdmin = req.body

  try {
    await registerAdminValidateSchema.validateAsync(reqAdmin)

    const salt = await bcrypt.genSalt(10)

    const hashPassword = await bcrypt.hash(reqAdmin.password, salt)

    reqAdmin.password = hashPassword

    const newAdmin = await User.create(reqAdmin)

    // Creates and signes a JWT for the newly created admin user.
    const accessToken = jwt.sign(
      {
        sub: newAdmin._id,
        firstName: newAdmin.firstName,
        lastName: newAdmin.lastName,
        username: newAdmin.username,
        type: 'access'
      },
      process.env.TOKEN_SECRET,
      {
        expiresIn: '12h'
      }
    )

    return res.status(201).send({
      data: {
        message: `The user ${newAdmin.username} has been created and logged in!`,
        admin: {
          id: newAdmin._id,
          fullName: newAdmin.firstName + ' ' + newAdmin.lastName,
          username: newAdmin.username,
          email: newAdmin.email
        },
        token: `Bearer ${accessToken}`
      }
    })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(400).send({
        data: {
          error: {
            name: 'Validation error',
            message: error.details[0].message
          }
        }
      })
    }

    if (error.name === 'MongoError') {
      return res.status(500).send({
        data: {
          error: {
            name: 'Mongo error',
            message: error.message
          }
        }
      })
    }
    return res.status(500).send({
      data: {
        error: {
          error: error.name,
          message: 'An internal error occured.'
        }
      }
    })
  }
}

export async function updateAdmin (req, res) {
  const adminId = req.params.id
  const reqAdmin = req.body
  const query = {
    _id: adminId
  }

  try {
    await updateAdminValidateSchema.validateAsync(reqAdmin)

    reqAdmin.updatedAt = new Date()

    // TODO: updateResponse.n and updateResponse.nModified must be equal in tests!
    const updateResponse = await User.updateOne(query, reqAdmin)

    if (updateResponse !== 1 && updateResponse.nModified !== 1) {
      throw new AdminError('The user could not be updated!')
    }

    res.status(200).send({
      data: {
        message: `Done! Updated user with ID:${adminId}`
      }
    })
  } catch (error) {
    if (error.name === 'ValidationError') {
      return res.status(400).send({
        data: {
          error: {
            name: 'Validation error',
            message: error.details[0].message
          }
        }
      })
    }

    if (error.name === 'AdminError') {
      return res.status(403).send({
        data: {
          error: {
            name: 'User error',
            message: error.message
          }
        }
      })
    }
    return res.status(500).send({
      data: {
        error: error.name,
        message: 'An internal error occured.'
      }
    })
  }
}

export async function softDeleteAdmin (req, res) {
  const adminId = req.params.id
  const query = {
    _id: adminId
  }

  try {
    const admin = await User.findById(adminId)
    admin.updatedAt = new Date()
    admin.deletedAt = new Date()

    const updateResponse = await User.updateOne(query, admin)

    if (updateResponse !== 1 && updateResponse.nModified !== 1) {
      throw new AdminError('The user could not be deleted!')
    }

    res.status(200).send({
      data: {
        message: `Done! deleted user with ID:${adminId}`
      }
    })
  } catch (error) {
    if (error.name === 'AdminError') {
      return res.status(403).send({
        data: {
          error: {
            name: 'User error',
            message: error.message
          }
        }
      })
    }

    return res.status(500).send({
      data: {
        error: error.name,
        message: 'An internal error occured.'
      }
    })
  }
}
