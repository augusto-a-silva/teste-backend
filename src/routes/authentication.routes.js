import * as authenticationController from '../controllers/authentication.controller.js'

export function router (router) {
  router.route('/auth/login')
    .post(authenticationController.login)

  // TODO adds a logout route

  return router
}
