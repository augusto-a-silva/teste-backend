import express from 'express'
import * as userRoutes from './user.routes.js'
import * as authenticationRoutes from './authentication.routes.js'
import * as adminRoutes from './admin.routes.js'
import * as movieRoutes from './movie.routes.js'

const router = express.Router()

export function initialize (app) {
  /*
   * Setting up routes for the routers.
   */
  const userRouter = userRoutes.router(router)
  const authenticationRouter = authenticationRoutes.router(router)
  const adminRouter = adminRoutes.router(router)
  const movieRouter = movieRoutes.router(router)

  /*
   * Mounting up the router into the express application.
   */
  app.use('/api/v1', userRouter)
  app.use('/api/v1', authenticationRouter)
  app.use('/api/v1', adminRouter)
  app.use('/api/v1', movieRouter)
}
