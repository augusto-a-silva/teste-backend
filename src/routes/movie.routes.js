import * as movieController from '../controllers/movie.controller.js'
import { isBearerValid, isAdmin, isNotAdmin } from '../middlewares/authorization.middleware.js'

function router (router) {
  router.route('/movies')
    .get([isBearerValid], movieController.searchMovies)

  router.route('/movie/:id')
    .get([isBearerValid], movieController.searchMovie)

  router.route('/movie')
    .post([isBearerValid, isAdmin], movieController.registerMovie)

  router.route('/movie/rate/:id')
    .post([isBearerValid, isNotAdmin], movieController.rateMovie)

  return router
}

export { router }
