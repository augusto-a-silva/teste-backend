import * as adminController from '../controllers/admin.controller.js'
import { isBearerValid, isAdmin } from '../middlewares/authorization.middleware.js'

function router (router) {
  router.route('/admin/user')
    .post([isBearerValid, isAdmin], adminController.registerAdmin)

  router.route('/admin/user/:id')
    .put([isBearerValid, isAdmin], adminController.updateAdmin)
    .delete([isBearerValid, isAdmin], adminController.softDeleteAdmin)

  return router
}

export { router }
