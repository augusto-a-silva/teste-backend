import * as userController from '../controllers/user.controller.js'
import { isBearerFromSameUser, isBearerValid } from '../middlewares/authorization.middleware.js'

function router (router) {
  router.route('/user')
    .post(userController.registerUser)

  router.route('/user/:id')
    .put([isBearerValid, isBearerFromSameUser], userController.updateUser)
    .delete([isBearerValid, isBearerFromSameUser], userController.softDeleteUser)

  router.route('/user/change-password/:id')
    .put([isBearerValid, isBearerFromSameUser], userController.updateUserPassword)


  return router
}

export { router }
