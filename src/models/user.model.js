import mongoose from 'mongoose'

const UserSchema = mongoose.Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  isAdmin: {
    type: Boolean,
    required: true,
    default: 'false'
  },
  createdAt: {
    type: Date,
    required: true,
    default: Date.now()
  },
  updatedAt: {
    type: Date
  },
  deletedAt: {
    type: Date
  }
})

const User = mongoose.model('User', UserSchema)

export { User }
