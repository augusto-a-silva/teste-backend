import mongoose from 'mongoose'

const MovieSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  genre: {
    type: String,
    required: true
  },
  director: {
    type: String
  },
  writers: [String],
  actors: [String],
  rates: [{
    ratedBy: String,
    rating: {
      type: Number,
      enum: [0, 1, 2, 3, 4]
    }
  }],
  avgRate: {
    type: Number,
    default: 0
  },
  synopsis: {
    type: String,
    required: true
  },
  year: {
    type: Number
  }

})

const Movie = mongoose.model('Movie', MovieSchema)

export { Movie }
