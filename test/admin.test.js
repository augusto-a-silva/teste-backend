import chai from 'chai'
import chaiHttp from 'chai-http'
import axios from 'axios'
import app from '../src/server.js'
import { User } from '../src/models/user.model.js'

chai.use(chaiHttp)
const expect = chai.expect
const assert = chai.assert

describe('Admin endpoints tests:', () => {
  let sysAdmin
  let sysAdminBearer
  let testAdminOne
  let testAdminTwo
  let bearerAdminTestOne
  let bearerAdminTestTwo

  before(async () => {
    await User.deleteOne({ email: 'admintest01@gmail.com' })
    await User.deleteOne({ email: 'admintest02@gmail.com' })

    return axios.post('http://localhost:3000/api/v1/auth/login', {
      email: 'systemadmin@email.com',
      password: 'sysadmin123'
    }).then((res) => {
      // eslint-disable-next-line no-unused-vars
      sysAdmin = res.data.data.user
      sysAdminBearer = res.data.data.token
    })
  })

  after(async () => {
    await User.deleteOne({ email: 'admintest01@gmail.com' })
    await User.deleteOne({ email: 'admintest02@gmail.com' })
  })

  describe('Test admin endpoints for successful requests:', () => {
    describe('[POST]/admin/user', () => {
      it('Sould return a http status code of 201.', (done) => {
        chai.request(app)
          .post('/api/v1/admin/user')
          .set('Authorization', sysAdminBearer)
          .send({
            firstName: 'Administrator',
            lastName: 'Test 01',
            username: 'testadmin01',
            email: 'admintest01@gmail.com',
            password: 'admin123',
            isAdmin: true
          })
          .end((_err, res) => {
            expect(res).to.have.status(201)
            testAdminOne = res.body.data.admin
            bearerAdminTestOne = res.body.data.token
            done()
          })
      })

      it('Sould return a JSON with message; user\'s id, fullname, fullname, and email; and token.', (done) => {
        chai.request(app)
          .post('/api/v1/admin/user')
          .set('Authorization', sysAdminBearer)
          .send({
            firstName: 'Administrator',
            lastName: 'Test 02',
            username: 'testadmin02',
            email: 'admintest02@gmail.com',
            password: 'admin123',
            isAdmin: true
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('message')
            expect(res.body.data).to.have.property('admin')
            expect(res.body.data).to.have.property('token')
            expect(res.body.data.message).to.be.a('string')
            expect(res.body.data.admin).to.be.a('object')
            expect(res.body.data.token).to.be.a('string')
            assert.isOk((res.body.data.token.startsWith('bearer'), 'The token is invalid.'))
            testAdminTwo = res.body.data.admin
            bearerAdminTestTwo = res.body.data.token
            done()
          })
      })
    })

    describe('[PUT]/admin/user', () => {
      it('Sould return a http status code of 200.', (done) => {
        chai.request(app)
          .put('/api/v1/admin/user/' + testAdminOne.id)
          .set('Authorization', bearerAdminTestOne)
          .send({
            firstName: 'Administrator',
            lastName: 'Test 01',
            username: 'admintest01',
            email: 'admintest01@gmail.com',
            password: 'admin123',
            isAdmin: true
          })
          .end((_err, res) => {
            expect(res).to.have.status(200)
            done()
          })
      })

      it('Sould return a JSON with a message that says the user was updated.', (done) => {
        chai.request(app)
          .put('/api/v1/admin/user/' + testAdminTwo.id)
          .set('Authorization', bearerAdminTestTwo)
          .send({
            firstName: 'Administrator',
            lastName: 'Test 02',
            username: 'admintest02',
            email: 'admintest02@gmail.com',
            password: 'admin123',
            isAdmin: true
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('message')
            expect(res.body.data.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[DELETE]/admin/user', () => {
      it('Sould return a http status code of 200.', (done) => {
        chai.request(app)
          .delete('/api/v1/admin/user/' + testAdminOne.id)
          .set('Authorization', bearerAdminTestOne)
          .end((_err, res) => {
            expect(res).to.have.status(200)
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('message')
            expect(res.body.data.message).to.be.a('string')
            done()
          })
      })

      it('Sould return a JSON with a message that says the user was delete', (done) => {
        chai.request(app)
          .delete('/api/v1/admin/user/' + testAdminTwo.id)
          .set('Authorization', bearerAdminTestTwo)
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('message')
            expect(res.body.data.message).to.be.a('string')
            done()
          })
      })
    })
  })

  describe('Test admin endpoints for unsuccessful requests:', () => {
    describe('[POST]/admin/user - Any, or all, missing, or invalid, field', () => {
      it('Sould return a http status code of 400.', (done) => {
        chai.request(app)
          .post('/api/v1/admin/user')
          .set('Authorization', sysAdminBearer)
          .send({
            firstName: '',
            lastName: '',
            username: '',
            email: '@gmail',
            password: ''
          })
          .end((_err, res) => {
            expect(res).to.have.status(400)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message.', (done) => {
        chai.request(app)
          .post('/api/v1/admin/user')
          .set('Authorization', sysAdminBearer)
          .send({
            firstName: '',
            lastName: '',
            username: '',
            email: '@gmail',
            password: ''
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[POST]/admin/user - Duplicated email', () => {
      it('Sould return a http status code of 500.', (done) => {
        chai.request(app)
          .post('/api/v1/admin/user')
          .set('Authorization', sysAdminBearer)
          .send({
            firstName: 'Administrator',
            lastName: 'Test 01',
            username: 'admintest0101',
            email: 'admintest01@gmail.com',
            password: 'admin123',
            isAdmin: true
          })
          .end((_err, res) => {
            expect(res).to.have.status(500)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message.', (done) => {
        chai.request(app)
          .post('/api/v1/admin/user')
          .set('Authorization', sysAdminBearer)
          .send({
            firstName: 'Administrator',
            lastName: 'Test 02',
            username: 'admintest0202',
            email: 'admintest02@gmail.com',
            password: 'admin123',
            isAdmin: true
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[POST]/admin/user - Duplicated username', () => {
      it('Sould return a http status code of 500.', (done) => {
        chai.request(app)
          .post('/api/v1/admin/user')
          .set('Authorization', sysAdminBearer)
          .send({
            firstName: 'Administrator',
            lastName: 'Test 01',
            username: 'admintest01',
            email: 'admintest0101@gmail.com',
            password: 'admin123',
            isAdmin: true
          })
          .end((_err, res) => {
            expect(res).to.have.status(500)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message.', (done) => {
        chai.request(app)
          .post('/api/v1/admin/user')
          .set('Authorization', sysAdminBearer)
          .send({
            firstName: 'Administrator',
            lastName: 'Test 01',
            username: 'admintest01',
            email: 'admintest0101@gmail.com',
            password: 'admin123',
            isAdmin: true
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[PUT]/user - Any, or all, missing, or invalid, field', () => {
      it('Sould return a http status code of 400.', (done) => {
        chai.request(app)
          .put('/api/v1/admin/user/' + testAdminTwo.id)
          .set('Authorization', sysAdminBearer)
          .send({
            firstName: '',
            lastName: '',
            username: '',
            email: '@gmail'
          })
          .end((_err, res) => {
            expect(res).to.have.status(400)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message.', (done) => {
        chai.request(app)
          .put('/api/v1/admin/user/' + testAdminTwo.id)
          .set('Authorization', sysAdminBearer)
          .send({
            firstName: '',
            lastName: '',
            username: '',
            email: '@gmail'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })
  })
})
