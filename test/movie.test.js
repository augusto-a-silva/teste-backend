/* eslint-disable no-unused-vars */
import axios from 'axios'
import chai from 'chai'
import chaiHttp from 'chai-http'
import app from '../src/server.js'
import { Movie } from '../src/models/movie.model.js'

chai.use(chaiHttp)
const expect = chai.expect

describe('Test movies endpoints:', () => {
  let sysAdmin
  let sysAdminBearer
  let user
  let userBearer
  let movieId

  before(async () => {
    const moviesFound = await Movie.find({})

    if (moviesFound.length === 0) {
      const movies = [
        {
          title: 'Mank',
          genre: 'Biography',
          director: 'David Fincher',
          writers: ['Jack Fincher'],
          actors: ['Gary Oldman', 'Amanda Seyfried', 'Lily Collins'],
          synopsis: "1930's Hollywood is reevaluated through the eyes of scathing social critic and alcoholic screenwriter Herman J. Mankiewicz as he races to finish the screenplay of Cidadão Kane (1941).",
          year: 2020
        },
        {
          title: 'Soul',
          genre: 'Animation',
          director: 'Pete Docter',
          writers: ['Kemp Powers'],
          actors: ['Jamie Foxx', 'Tina Fey', 'Graham Norton', 'Rachel House'],
          synopsis: 'After landing the gig of a lifetime, a New York jazz pianist suddenly finds himself trapped in a strange land between Earth and the afterlife..',
          year: 2020
        },
        {
          title: 'A Subida',
          genre: 'Biography',
          director: 'David Fincher',
          writers: ['Jack Fincher'],
          actors: ['Gary Oldman', 'Amanda Seyfried'],
          synopsis: 'A look at the friendship between two guys that spans over many years.',
          year: 2019
        }
      ]
      await Movie.insertMany(movies)
    }

    axios.post('http://localhost:3000/api/v1/auth/login', {
      email: 'systemadmin@email.com',
      password: 'sysadmin123'
    }).then((res) => {
      sysAdmin = res.data.data.user
      sysAdminBearer = res.data.data.token
    })

    return axios.post('http://localhost:3000/api/v1/auth/login', {
      email: 'joaosilva@email.com',
      password: 'joao123'
    }).then((res) => {
      user = res.data.data.user
      userBearer = res.data.data.token
    })
  })

  after(async () => {
    await Movie.deleteOne({ title: 'A Subida - FAKE', director: 'David Fincher' })
    await Movie.deleteOne({ title: 'A Descida - FAKE', director: 'David Fincher' })
  })
  describe('Test movies endpoints for successful requests:', () => {
    describe('[GET]/movies', () => {
      it('Sould return a http status code of 200.', (done) => {
        chai.request(app)
          .get('/api/v1/movies')
          .set('Authorization', userBearer)
          .query({
            director: '',
            actors: ['Gary Oldman', 'Amanda Seyfried']
          })
          .end((_err, res) => {
            expect(res).to.have.status(200)
            movieId = res.body.data.movies[0]._id
            done()
          })
      })

      it('Sould return a JSON with the movies array.', (done) => {
        chai.request(app)
          .get('/api/v1/movies')
          .set('Authorization', userBearer)
          .query({
            director: '',
            title: 'Soul'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('movies')
            expect(res.body.data.movies).to.be.a('array')
            done()
          })
      })
    })

    describe('[GET]/movie', () => {
      it('Sould return a http status code of 200.', (done) => {
        chai.request(app)
          .get('/api/v1/movie/' + movieId)
          .set('Authorization', userBearer)
          .end((_err, res) => {
            expect(res).to.have.status(200)
            done()
          })
      })

      it('Sould return a JSON with the movie metadata.', (done) => {
        chai.request(app)
          .get('/api/v1/movie/' + movieId)
          .set('Authorization', userBearer)
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('movie')
            expect(res.body.data.movie).to.be.a('object')
            expect(res.body.data.movie).to.property('director')
            expect(res.body.data.movie).to.property('title')
            expect(res.body.data.movie).to.property('actors')
            expect(res.body.data.movie).to.property('genre')
            done()
          })
      })
    })

    describe('[POST]/movie', () => {
      it('Sould return a http status code of 200.', (done) => {
        chai.request(app)
          .post('/api/v1/movie')
          .set('Authorization', sysAdminBearer)
          .send({
            title: 'A Subida - FAKE',
            genre: 'Biography',
            director: 'David Fincher',
            writers: ['Jack Fincher'],
            actors: ['Gary Oldman', 'Amanda Seyfried'],
            synopsis: 'A look at the friendship between two guys that spans over many years.',
            year: 2019
          })
          .end((_err, res) => {
            expect(res).to.have.status(201)
            done()
          })
      })

      it('Sould return a JSON with the movie metadata and a message.', (done) => {
        chai.request(app)
          .post('/api/v1/movie')
          .set('Authorization', sysAdminBearer)
          .send({
            title: 'A Descida - FAKE',
            genre: 'Biography',
            director: 'David Fincher',
            writers: ['Jack Fincher'],
            actors: ['Gary Oldman', 'Amanda Seyfried'],
            synopsis: 'A look at the friendship between two guys that spans over many years.',
            year: 2019
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('message')
            expect(res.body.data.message).to.be.a('string')
            expect(res.body.data).to.have.property('movie')
            expect(res.body.data.movie).to.be.a('object')
            expect(res.body.data.movie).to.property('director')
            expect(res.body.data.movie).to.property('title')
            expect(res.body.data.movie).to.property('actors')
            expect(res.body.data.movie).to.property('genre')
            done()
          })
      })
    })

    describe('[PUT]/movie', () => {
      it('Sould return a http status code of 200.', (done) => {
        chai.request(app)
          .put('/api/v1/movie/rate/' + movieId)
          .set('Authorization', userBearer)
          .send({
            rating: 4,
            ratedBy: user._id
          })
          .end((_err, res) => {
            expect(res).to.have.status(200)
            done()
          })
      })

      it('Sould return a JSON with the movie metadata.', (done) => {
        chai.request(app)
          .put('/api/v1/movie/rate/' + movieId)
          .set('Authorization', userBearer)
          .send({
            rating: 3,
            ratedBy: user._id
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('movie')
            expect(res.body.data.movie).to.be.a('object')
            expect(res.body.data.movie).to.property('director')
            expect(res.body.data.movie).to.property('title')
            expect(res.body.data.movie).to.property('actors')
            expect(res.body.data.movie).to.property('genre')
            expect(res.body.data.movie).to.property('avgRate')
            done()
          })
      })
    })
  })
})
