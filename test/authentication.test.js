import bcrypt from 'bcrypt'
import chai from 'chai'
import chaiHttp from 'chai-http'
import app from '../src/server.js'
import { User } from '../src/models/user.model.js'

chai.use(chaiHttp)
const expect = chai.expect
const assert = chai.assert

describe('Test authentication endpoints:', () => {
  describe('Test endpoints for successful requests:', () => {
    before(async () => {
      const newTestUserOne = {
        firstName: 'José',
        lastName: 'Maria',
        username: 'josemaria',
        email: 'josemaria@gmail.com',
        password: 'jose123'
      }

      const newTestUserTwo = {
        firstName: 'José',
        lastName: 'Ninguem',
        username: 'joseninguem',
        email: 'joseninguem@gmail.com',
        password: 'jose123'
      }

      const salt = await bcrypt.genSalt(10)

      const hashPasswordOne = await bcrypt.hash(newTestUserOne.password, salt)
      const hashPasswordTwo = await bcrypt.hash(newTestUserOne.password, salt)

      newTestUserOne.password = hashPasswordOne
      newTestUserTwo.password = hashPasswordTwo

      await User.create(newTestUserOne)
      await User.create(newTestUserTwo)
    })

    after(async () => {
      await User.deleteOne({ email: 'josemaria@gmail.com' })
      await User.deleteOne({ email: 'joseninguem@gmail.com' })
    })

    describe('[POST]/auth/login', () => {
      it('Sould return a http status code of 200.', (done) => {
        chai.request(app)
          .post('/api/v1/auth/login')
          .send({
            email: 'joseninguem@gmail.com',
            password: 'jose123'
          })
          .end((_err, res) => {
            expect(res).to.have.status(200)
            done()
          })
      })

      it('Sould return a JSON with message; user\'s fullname, fullname, and email; and token', (done) => {
        chai.request(app)
          .post('/api/v1/auth/login')
          .send({
            email: 'josemaria@gmail.com',
            password: 'jose123'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('message')
            expect(res.body.data).to.have.property('user')
            expect(res.body.data).to.have.property('token')
            expect(res.body.data.message).to.be.a('string')
            expect(res.body.data.user).to.be.a('object')
            expect(res.body.data.token).to.be.a('string')
            assert.isOk((res.body.data.token.startsWith('bearer'), 'The token is invalid.'))
            done()
          })
      })
    })
  })

  describe('Test endpoints for unsuccessful requests:', () => {
    before(async () => {
      const newTestUserOne = {
        firstName: 'José',
        lastName: 'Maria',
        username: 'josemaria',
        email: 'josemaria@gmail.com',
        password: 'jose123'
      }

      const newTestUserTwo = {
        firstName: 'José',
        lastName: 'Ninguem',
        username: 'joseninguem',
        email: 'joseninguem@gmail.com',
        password: 'jose123'
      }

      const salt = await bcrypt.genSalt(10)

      const hashPasswordOne = await bcrypt.hash(newTestUserOne.password, salt)
      const hashPasswordTwo = await bcrypt.hash(newTestUserOne.password, salt)

      newTestUserOne.password = hashPasswordOne
      newTestUserTwo.password = hashPasswordTwo

      await User.create(newTestUserOne)
      await User.create(newTestUserTwo)
    })

    after(async () => {
      await User.deleteOne({ email: 'josemaria@gmail.com' })
      await User.deleteOne({ email: 'joseninguem@gmail.com' })
    })

    describe('[POST]/auth/login - Wrong email.', () => {
      it('Sould return a http status code of 404.', (done) => {
        chai.request(app)
          .post('/api/v1/auth/login')
          .send({
            email: 'joseninguem1@gmail.com',
            password: 'jose123'
          })
          .end((_err, res) => {
            expect(res).to.have.status(404)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message.', (done) => {
        chai.request(app)
          .post('/api/v1/auth/login')
          .send({
            email: 'josemaria1@gmail.com',
            password: 'jose123'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[POST]/auth/login - Wrong password.', () => {
      it('Sould return a http status code of 401.', (done) => {
        chai.request(app)
          .post('/api/v1/auth/login')
          .send({
            email: 'joseninguem@gmail.com',
            password: 'jose1234'
          })
          .end((_err, res) => {
            expect(res).to.have.status(401)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message.', (done) => {
        chai.request(app)
          .post('/api/v1/auth/login')
          .send({
            email: 'josemaria@gmail.com',
            password: 'jose1234'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })
  })
})
