import chai from 'chai'
import chaiHttp from 'chai-http'
import app from '../src/server.js'
import { User } from '../src/models/user.model.js'

chai.use(chaiHttp)
const expect = chai.expect
const assert = chai.assert

describe('Users endpoints tests:', () => {
  let testUserOne
  let testUserTwo
  let testUserBearerOne
  let testUserBearerTwo

  before(async () => {
    await User.deleteOne({ email: 'josemaria@gmail.com' })
    await User.deleteOne({ email: 'joseninguem@gmail.com' })
  })

  after(async () => {
    await User.deleteOne({ email: 'josemaria@gmail.com' })
    await User.deleteOne({ email: 'joseninguem@gmail.com' })
  })

  describe('Test endpoints for successful requests:', () => {
    describe('[POST]/user', () => {
      it('Sould return a http status code of 201.', (done) => {
        chai.request(app)
          .post('/api/v1/user')
          .send({
            firstName: 'José',
            lastName: 'Ninguém',
            username: 'joseninguem',
            email: 'joseninguem@gmail.com',
            password: 'jose123'
          })
          .end((_err, res) => {
            expect(res).to.have.status(201)
            testUserOne = res.body.data.user
            testUserBearerOne = res.body.data.token
            done()
          })
      })

      it('Sould return a JSON with message; user\'s id, fullname, fullname, and email; and token.', (done) => {
        chai.request(app)
          .post('/api/v1/user')
          .send({
            firstName: 'José',
            lastName: 'Maria',
            username: 'josemaria',
            email: 'josemaria@gmail.com',
            password: 'jose123'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('message')
            expect(res.body.data).to.have.property('user')
            expect(res.body.data).to.have.property('token')
            expect(res.body.data.message).to.be.a('string')
            expect(res.body.data.user).to.be.a('object')
            expect(res.body.data.token).to.be.a('string')
            assert.isOk((res.body.data.token.startsWith('bearer'), 'The token is invalid.'))
            testUserTwo = res.body.data.user
            testUserBearerTwo = res.body.data.token
            done()
          })
      })
    })

    describe('[PUT]/user', () => {
      it('Sould return a http status code of 200.', (done) => {
        chai.request(app)
          .put('/api/v1/user/' + testUserOne.id)
          .set('Authorization', testUserBearerOne)
          .send({
            firstName: 'José',
            lastName: 'Ninguem',
            username: 'ninguemjose',
            email: 'joseninguem@gmail.com'
          })
          .end((_err, res) => {
            expect(res).to.have.status(200)
            done()
          })
      })

      it('Sould return a JSON with a message that says the user was updated.', (done) => {
        chai.request(app)
          .put('/api/v1/user/' + testUserTwo.id)
          .set('Authorization', testUserBearerTwo)
          .send({
            firstName: 'José',
            lastName: 'Maria',
            username: 'mariajose',
            email: 'josemaria@gmail.com'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('message')
            expect(res.body.data.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[PUT]/user/change-password', () => {
      it('Sould return a http status code of 200.', (done) => {
        chai.request(app)
          .put('/api/v1/user/change-password/' + testUserOne.id)
          .set('Authorization', testUserBearerOne)
          .send({
            currentPassword: 'jose123',
            newPassword: 'jose1234',
            confirmPassword: 'jose1234'
          })
          .end((_err, res) => {
            expect(res).to.have.status(200)
            done()
          })
      })

      it('Sould return a JSON with a message that says the user has it password updated.', (done) => {
        chai.request(app)
          .put('/api/v1/user/change-password/' + testUserOne.id)
          .set('Authorization', testUserBearerOne)
          .send({
            currentPassword: 'jose1234',
            newPassword: 'jose12345',
            confirmPassword: 'jose12345'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('message')
            expect(res.body.data.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[DELETE]/user', () => {
      it('Sould return a http status code of 200.', (done) => {
        chai.request(app)
          .delete('/api/v1/user/' + testUserOne.id)
          .set('Authorization', testUserBearerOne)
          .end((_err, res) => {
            expect(res).to.have.status(200)
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('message')
            expect(res.body.data.message).to.be.a('string')
            done()
          })
      })

      it('Sould return a JSON with a message that says the user was delete', (done) => {
        chai.request(app)
          .delete('/api/v1/user/' + testUserTwo.id)
          .set('Authorization', testUserBearerTwo)
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('message')
            expect(res.body.data.message).to.be.a('string')
            done()
          })
      })
    })
  })

  describe('Test endpoints for unsuccessful requests:', () => {
    describe('[POST]/user - Any, or all, missing, or invalid, field', () => {
      it('Sould return a http status code of 400.', (done) => {
        chai.request(app)
          .post('/api/v1/user')
          .send({
            firstName: '',
            lastName: '',
            username: '',
            email: '',
            password: ''
          })
          .end((_err, res) => {
            expect(res).to.have.status(400)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message.', (done) => {
        chai.request(app)
          .post('/api/v1/user')
          .send({
            firstName: '',
            lastName: '',
            username: '',
            email: '',
            password: ''
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[POST]/user - Duplicated email', () => {
      it('Sould return a http status code of 500.', (done) => {
        chai.request(app)
          .post('/api/v1/user')
          .send({
            firstName: 'José',
            lastName: 'Ninguém',
            username: 'joseninguem1',
            email: 'joseninguem@gmail.com',
            password: 'jose123'
          })
          .end((_err, res) => {
            expect(res).to.have.status(500)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message.', (done) => {
        chai.request(app)
          .post('/api/v1/user')
          .send({
            firstName: 'José',
            lastName: 'Ninguém',
            username: 'joseninguem1',
            email: 'joseninguem@gmail.com',
            password: 'jose123'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[POST]/user - Duplicated username', () => {
      it('Sould return a http status code of 500.', (done) => {
        chai.request(app)
          .post('/api/v1/user')
          .send({
            firstName: 'José',
            lastName: 'Ninguém',
            username: 'ninguemjose',
            email: 'joseninguem1@gmail.com',
            password: 'jose123'
          })
          .end((_err, res) => {
            expect(res).to.have.status(500)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message.', (done) => {
        chai.request(app)
          .post('/api/v1/user')
          .send({
            firstName: 'José',
            lastName: 'Ninguém',
            username: 'ninguemjose',
            email: 'joseninguem1@gmail.com',
            password: 'jose123'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[PUT]/user - Any, or all, missing, or invalid, field', () => {
      it('Sould return a http status code of 400.', (done) => {
        chai.request(app)
          .put('/api/v1/user/' + testUserTwo.id)
          .set('Authorization', testUserBearerTwo)
          .send({
            firstName: '',
            lastName: '',
            username: '',
            email: '@gmail'
          })
          .end((_err, res) => {
            expect(res).to.have.status(400)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message.', (done) => {
        chai.request(app)
          .put('/api/v1/user/' + testUserTwo.id)
          .set('Authorization', testUserBearerTwo)
          .send({
            firstName: '',
            lastName: '',
            username: '',
            email: '@gmail'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[PUT]/user - Logged user requesting the update of a differente user.', () => {
      it('Sould return a http status code of 401.', (done) => {
        chai.request(app)
          .put('/api/v1/user/' + testUserTwo.id)
          .set('Authorization', testUserBearerOne)
          .send({
            firstName: 'José',
            lastName: 'Ninguém',
            username: 'joseninguem',
            email: 'joseninguem@gmail.com'
          })
          .end((_err, res) => {
            expect(res).to.have.status(401)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message.', (done) => {
        chai.request(app)
          .put('/api/v1/user/' + testUserTwo.id)
          .set('Authorization', testUserBearerOne)
          .send({
            firstName: 'José',
            lastName: 'Ninguém',
            username: 'joseninguem',
            email: 'joseninguem@gmail.com'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[PUT]/user/change-password - Different password.', () => {
      it('Sould return a http status code of 400.', (done) => {
        chai.request(app)
          .put('/api/v1/user/change-password/' + testUserOne.id)
          .set('Authorization', testUserBearerOne)
          .send({
            currentPassword: 'jose123',
            newPassword: 'jose1234',
            confirmPassword: 'jose1234'
          })
          .end((_err, res) => {
            expect(res).to.have.status(400)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message', (done) => {
        chai.request(app)
          .put('/api/v1/user/change-password/' + testUserOne.id)
          .set('Authorization', testUserBearerOne)
          .send({
            currentPassword: 'jose123',
            newPassword: 'jose1234',
            confirmPassword: 'jose1234'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[PUT]/user/change-password - New password equals to current password.', () => {
      it('Sould return a http status code of 400.', (done) => {
        chai.request(app)
          .put('/api/v1/user/change-password/' + testUserOne.id)
          .set('Authorization', testUserBearerOne)
          .send({
            currentPassword: 'jose12345',
            newPassword: 'jose12345',
            confirmPassword: 'jose12345'
          })
          .end((_err, res) => {
            expect(res).to.have.status(400)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message', (done) => {
        chai.request(app)
          .put('/api/v1/user/change-password/' + testUserOne.id)
          .set('Authorization', testUserBearerOne)
          .send({
            currentPassword: 'jose12345',
            newPassword: 'jose12345',
            confirmPassword: 'jose12345'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[PUT]/user/change-password - Logged user requesting the update of a differente user password.', () => {
      it('Sould return a http status code of 401.', (done) => {
        chai.request(app)
          .put('/api/v1/user/change-password/' + testUserOne.id)
          .set('Authorization', testUserBearerTwo)
          .send({
            currentPassword: 'jose123',
            newPassword: 'jose1234',
            confirmPassword: 'jose1234'
          })
          .end((_err, res) => {
            expect(res).to.have.status(401)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message', (done) => {
        chai.request(app)
          .put('/api/v1/user/change-password/' + testUserOne.id)
          .set('Authorization', testUserBearerTwo)
          .send({
            currentPassword: 'jose1234',
            newPassword: 'jose12345',
            confirmPassword: 'jose12345'
          })
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })

    describe('[DELETE]/user - Logged user requesting the deletion of a differente user.', () => {
      it('Sould return a http status code of 401.', (done) => {
        chai.request(app)
          .delete('/api/v1/user/' + testUserTwo.id)
          .set('Authorization', testUserBearerOne)
          .end((_err, res) => {
            expect(res).to.have.status(401)
            done()
          })
      })

      it('Sould return a JSON with error\'s name and message.', (done) => {
        chai.request(app)
          .delete('/api/v1/user/' + testUserTwo.id)
          .set('Authorization', testUserBearerOne)
          .end((_err, res) => {
            expect(res.body.data).to.be.a('object')
            expect(res.body.data).to.have.property('error')
            expect(res.body.data.error).to.be.a('object')
            expect(res.body.data.error).to.have.property('name')
            expect(res.body.data.error).to.have.property('message')
            expect(res.body.data.error.name).to.be.a('string')
            expect(res.body.data.error.message).to.be.a('string')
            done()
          })
      })
    })
  })
})
