# Backend Test for ioasys

A test for ioasys selection. To learn more about the project requests, [read here (available in Portuguese only)](https://bitbucket.org/ioasys/teste-backend/src/master/).

## Running the project

Just clone the repository:

```
git clone https://augusto-a-silva@bitbucket.org/augusto-a-silva/teste-backend.git
```

Then copy the `.env.example` file to `.env` and edit it to you need.

Finally run the following command to get docker up:

```
docker-compose up --build
```

The project will run on `http://localhost`.

## Making requests

Import the postman's collection file and start making requests in it. For more details on how endpoints works, see the tests.

## Running the tests

To run the tests, make sure that the MONGO_URI variable in the .env file is set correctly and the mongo instance is up and available.

Then just run:

```
npm test
```

